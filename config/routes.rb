Rails.application.routes.draw do


  root 'urls#index'
  resources :urls
  get 'c/:code', to:'portal#redirector'

  get '/login', to: 'auth#index'
  post '/login', to: 'auth#login'
  delete '/logout', to: 'auth#delete'
  get 'register', to: 'users#register'
  get 'users', to: 'users#index'
  post 'register', to: 'users#create'


  namespace :api do
    resources :urls
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
