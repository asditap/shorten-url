class Url < ApplicationRecord
  validates :origin, presence: true
  validates :code, presence: true, uniqueness: true
end
