class ApplicationController < ActionController::Base
  helper_method :current_user
  helper_method :logged_in?

  def authenticate
    current_session = session[:user_id]
    redirect_to login_path, notice: 'Please sign in' if current_session.blank?
  end

  def current_user
    User.find_by(id: session[:user_id])
  end

  def logged_in?
    !current_user.nil?
  end
end
