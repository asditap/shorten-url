class PortalController < ApplicationController
  def redirector
    @url = Url.find_by(code: params[:code])
    if @url
      redirect_to @url.origin
    else
      render :file => "#{Rails.root}/public/404.html",  :status => 404
    end
  end
end
