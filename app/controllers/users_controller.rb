class UsersController < ApplicationController
  def register
    @user = User.new
  end

  def index
    @user = User.all
  end


  def create
    @user = User.create(username: params[:username], password: params[:password])
    session[:user_id] = @user.id
    redirect_to '/'
  end

end
