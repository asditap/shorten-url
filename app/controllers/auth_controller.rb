class AuthController < ApplicationController
  before_action :authenticate, only: [:delete]

  def index

  end
  def login
    @user = User.find_by(username: params[:username])
    if @user && @user.authenticate(params[:password])
      session[:user_id] = @user.id
      redirect_to urls_path
    else
      redirect_to login_path
    end
  end

  def delete
    session.clear

    redirect_to login_path, notice: 'Sign out success!'
  end
end
