class Api::UrlsController < ActionController::API
    include Generator
    include ApiResponse
    
    before_action :get_header_attr
    
    
    def get_header_attr
        @secretKey = request.headers["Secret-Key"]
        
        if @secretKey != "8dD87eFH9o7nV"
            render json: { message: "Secret key does't valid!", code: 401}, status: 401 
        end

    end

    #GET /api/v1/create
    def create        
        @generatedStr = random_string(5)

        if params[:code]
            @code = params[:code]
        else
            @code =  @generatedStr
        end

        @url = Url.create(code:  @code, origin: params[:origin])

        if @url.valid?
            render json: { message: "Url successfully store", data: @url}, status: 200 
        else 
            render json: { error: "Unable to store the URL",  errors: @url.errors}, status: 400 
        end

    end

    #PUT /api/v1/:id
    def update
        @url = Url.find(params[:id])
        if @url
            @url.update(code:  @code, origin: params[:origin])
            render json: { message: "Url successfully updated"}, status: 200 
        else 
            render json: { error: "Unable to update the URL"}, status: 400 
        end
    end

    def index
        @urls = Url.all 
        render json: {message: "Success load data", data: @urls}
    end

    def destroy
        @url = Url.find(params[:id])
        if @url
            @url.destroy
            render json: { message: "Success delete"}, status: 200 
        else 
            render json: { error: "Unable to delete"}, status: 400 
        end
    end

end
