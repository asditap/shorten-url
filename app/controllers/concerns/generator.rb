module Generator
    def random_string(length)
        charset = Array('A'..'Z') + Array(0..9)
        Array.new(length) { charset.sample }.join
    end
end