module ApiResponse
    def success(message, data)
        render json: {message: message, data: data}, status: 200
    end
    
    def error_invalid_parameters(exception)
        render json: { 'error': 'Invalid parameters: ' + exception.params.to_s },
               status: 422
    end
    
    def error_bad_request(exception)
        render json: { 'error': 'Bad Request',
                       'exception': exception.inspect }, status: 422
    end
    
    def error_record_invalid(exception)
        render json: { 'warning': 'Validation failed. :(. See exception for more' \
                                  'details. Generation aborted.',
                       "exception": exception.inspect,
                       "record": exception.record }, status: 201
      end
end