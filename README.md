# Shorten URL
Mini aplication to generate short link from your very long link 


## Requirements
* Ruby 3
* Rails  6 
* PosgreSQL 

## How to install
1. Clone repository to your local
2. run `bundle install && yarn install`
3. run  `cp config/application.yml.example config/application.yml` fill the config file
4. Setup database `rails db:setup && rails db:migrate && rails db:seed # Postgres need to be started`
5. run development server `rails s`

## How to use 
1. Register with your data
2. Login to dashboard 
3. Add link and get the short link 